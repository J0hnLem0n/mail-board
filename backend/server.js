const express = require("express");
const fs = require("fs");

const app = express();

app.use(express.static(__dirname + "/public"));
app.get("/api/mails", (req, res)=>{
    const content = fs.readFileSync("backend/mocks/mail.json", "utf8");
    const users = JSON.parse(content);
    res.send(users);
});

app.listen(3001, ()=>{
    console.log("server starting...");
});