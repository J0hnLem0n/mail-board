import React  from 'react';
import { render } from 'react-dom';
import MailBoard from './MailBoard'

import { Provider } from 'react-redux'
import { createStore, applyMiddleware  } from 'redux'
import mainReducers from './reducer'
import mainSaga  from './saga.js'
import createSagaMiddleware from 'redux-saga'

const sagaMiddleware = createSagaMiddleware();
const store = createStore(mainReducers, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(mainSaga);

render(
    <Provider store={store}>
        <MailBoard />
    </Provider>,
    document.getElementById('root')
);