import React, { Component } from 'react';
import { DragDropContext } from 'react-beautiful-dnd';
import DepartmentMailDesk from './Components/DepartmentMailDesk'
import Paper from 'material-ui/Paper';
import Grid from 'material-ui/Grid';
import { connect } from 'react-redux';

class MailBoardContainer extends Component {
    constructor(props) {
        super(props);
        this.onDragEnd = this.onDragEnd.bind(this);
    }

    componentWillMount() {
        this.props.dispatch( {type: 'GET_MAILS_FOR_MAIL_BOARD_REQUESTED'} );
    }

    reorderMails (list, result) {
        const categories = {
            technicalSupportMails: 'SET_TECHNICAL_SUPPORT_MAILS',
            marketingMails: 'SET_MARKETING_MAILS',
            seoMails: 'SET_SEO_MAILS',
            noSortMails: 'SET_NO_SORT_MAILS'
        };
        const removedArray = [...list[result.source.droppableId]];
        const pushedArray = [...list[result.destination.droppableId]];
        const [removedItem] = removedArray.splice(result.source.index, 1);

        this.props.dispatch( {type: categories[result.source.droppableId], payload: removedArray} );
        pushedArray.push(removedItem);
        this.props.dispatch( {type: categories[result.destination.droppableId], payload: pushedArray} );
    }

    onDragEnd(result) {
        if (!result.destination) {
            return;
        }
        this.reorderMails(
            this.props.mailBoard,
            result
        );

    }
    render() {
        const technicalSupportMails = this.props.mailBoard.technicalSupportMails;
        const marketingMails = this.props.mailBoard.marketingMails;
        const seoMails = this.props.mailBoard.seoMails;
        const noSortMails = this.props.mailBoard.noSortMails;

        return (
            <Grid container spacing={24}>
                <DragDropContext onDragEnd={this.onDragEnd}>
                    <Grid item xs={6}>
                        <Paper >
                            <DepartmentMailDesk droppableId={'noSortMails'} mailList={noSortMails} />
                        </Paper>
                    </Grid>
                    <Grid item xs={6}>
                        <Paper >
                            <DepartmentMailDesk droppableId={'technicalSupportMails'} mailList={technicalSupportMails} />
                            <DepartmentMailDesk droppableId={'marketingMails'} mailList={marketingMails} />
                            <DepartmentMailDesk droppableId={'seoMails'} mailList={seoMails} />
                        </Paper>
                    </Grid>
                </DragDropContext>
            </Grid>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        mailBoard: state.mailBoard
    }
};

const MailBoard = connect(
    mapStateToProps
)(MailBoardContainer);

export default MailBoard;