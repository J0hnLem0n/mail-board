const initialState = {
    technicalSupportMails: [],
    marketingMails: [],
    seoMails: [],
    noSortMails: []
};

const mailBoardReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_MAILS_FOR_MAIL_BOARD_REQUESTED':
            return state;
        case 'GET_MAILS_FOR_MAIL_BOARD_SUCCEEDED':
            return { ...state, noSortMails: action.payload };
        case 'GET_MAILS_FOR_MAIL_BOARD_FAILED':
            return state;
        case 'SET_MAILS_FOR_MAIL_BOARD':
            return { ...state, mails: action.payload };
        case 'SET_TECHNICAL_SUPPORT_MAILS':
            return { ...state, technicalSupportMails: action.payload };
        case 'SET_MARKETING_MAILS':
            return { ...state, marketingMails: action.payload };
        case 'SET_SEO_MAILS':
            return { ...state, seoMails: action.payload };
        case 'SET_NO_SORT_MAILS':
            return { ...state, noSortMails: action.payload };
        default:
            return state;
    }
};
export default mailBoardReducer