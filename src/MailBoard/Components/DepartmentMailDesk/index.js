import React, { Component } from 'react';
import { Droppable } from 'react-beautiful-dnd';
import MailDeskDragItem from './MailDeskDragItem';
import Grid from 'material-ui/Grid';
import './index.css';

class DepartmentMailDesk extends Component {
    render(){
        return(
            <Droppable droppableId={this.props.droppableId}>
                {(provided, snapshot) => (
                    <Grid container className="mail-desk">
                        <span className="mail-desk__header">{this.props.droppableId}</span>
                        <Grid container>
                            <Grid item xs={4}>
                                Summary
                            </Grid>
                            <Grid item xs={4}>
                                From email
                            </Grid>
                            <Grid item xs={4}>
                                Date
                            </Grid>
                            <div ref={provided.innerRef}
                                 className={ `mail-desk__drag-panel ${
                                     snapshot.isDraggingOver
                                         ? 'mail-desk__drag-panel_lightblue'
                                         : 'mail-desk__drag-panel_whitesmoke'}`} >
                                {this.props.mailList.map((item, index) => (
                                    <MailDeskDragItem key={ item.id } index={ index } mail={ item } />
                                ))}
                                {provided.placeholder}
                            </div>
                        </Grid>
                    </Grid>
                )}
            </Droppable>
        )}
}
export default DepartmentMailDesk;