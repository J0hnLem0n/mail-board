import React, { Component } from 'react';
import { Draggable } from 'react-beautiful-dnd';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import './index.css'

class MailDeskDragItem extends Component {
    getItemStyle = (isDragging, draggableStyle) => ({
        userSelect: 'none',
        ...draggableStyle,
    });
    render(){
        return(
            <Draggable key={this.props.mail.id} draggableId={this.props.mail.id} index={this.props.index}>
                {(provided, snapshot) => (
                    <div>
                        <div
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                            style={this.getItemStyle(
                                snapshot.isDragging,
                                provided.draggableProps.style
                            )}>

                            <Paper>
                            <Grid container className="drag-item">
                                <Grid item xs={4}>
                                    {`${this.props.mail.content.substring(0,30)}...`}
                                </Grid>
                                <Grid item xs={4}>
                                    {this.props.mail.email}
                                </Grid>
                                <Grid item xs={4}>
                                    {this.props.mail.date}
                                </Grid>

                            </Grid>
                            </Paper>
                        </div>
                        {provided.placeholder}
                    </div>
                )}
            </Draggable>
        )}
}
export default MailDeskDragItem;