import { combineReducers } from 'redux'
import mailBoard from './MailBoard/reducer'

const mainReducer = combineReducers({
    mailBoard
});

export default mainReducer