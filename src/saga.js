import { call, put, takeEvery } from 'redux-saga/effects'

function* getMails() {
    try {
        const mails = yield call((() => {
                return fetch("/api/mails")
                    .then(res => res.json())
            }
        ));
        yield put({type: "GET_MAILS_FOR_MAIL_BOARD_SUCCEEDED", payload: mails});
    } catch (e) {
        yield put({type: "GET_MAILS_FOR_MAIL_BOARD_FAILED", message: e.message});
    }
}

function* mainSaga() {
    yield takeEvery("GET_MAILS_FOR_MAIL_BOARD_REQUESTED", getMails);
}

export default mainSaga;